
## Installation

```shell
$ composer require webylum/puppeteer-server-client
```

Verify that the bundle as been added:

```php
// config/bundle.php

return [
//...
    Webylum\PuppeteerServerClient\WebylumPuppeteerServerClientBundle::class => ['all' => true],
//...
];
```


Create a new configuration file, if it does not already exists:

```yaml
# config/packages/webylum_puppeteer_server_client.yaml
webylum_puppeteer_server_client:
  server:
    host: http://localhost:10000 
 ```

## Usage

This bundle provide a service to create a client:

```php
use Webylum\PuppeteerServerClient\ClientInterface;

class MyController
{
    #[Route('/')]
    public function generatePdf(ClientInterface $client)
    {
        $payload = $client->createPayload();
        $payload
            ->setHtml('<h1>Hello World</h1>')
            ->setPdfOptions([
                'landscape' => true,            
            ])
            ->addAsset('LOGO', '/path/to/logo.png');
        $pdfResponse = $client->pdf($payload);
        
        $headers = array_flip(['content-type', 'content-length']);
        $headers = array_intersect_key($pdfResponse->getHeaders(), $headers);
        
        return new Response($pdfResponse->getContent(), 201, $headers);            
    }
}
```

The Client use the [Symfony HttpClient component](https://symfony.com/doc/current/http_client.html) and return a [Symfony HttpClient Response](https://github.com/symfony/http-client-contracts/blob/main/ResponseInterface.php) when generating pdf.

## Twig extension

This bundle provide a Twig extension to populate Payload assets:

```php
// Fox example, in a controller
$payload = $client->createPayload();
$html = $this->renderView('pdf/example.pdf', ['payload' => $payload]);
$payload->setHtml($html);
$result = $client->pdf($payload);
```

```twig
{# templates/pdf/example.pdf #}

<img src="{{ wpsc_register_asset(payload, asset('image.jpg')) }}" alt="Picture" style="width: 20%">
```

The extension will generate a placeholder, which will be used by the Puppeteer Server.

The placeholder is a sha1 hash of the asset url (returned by `{{ asset('path/to/my.asset') }}`)

```html
<-- Generation example -->
<img src="<##e603861aab88ad1170f2df5ed0d8bb7be0c84f02##>" alt="Picture" style="width: 20%">
```

When sending payload to the Puppeteer Server, the asset will be added as a form data part to the request.
