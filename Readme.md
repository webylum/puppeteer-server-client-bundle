# Puppeteer Server Client Bundle

This bundle provide a client to request a [Puppeteer Server](https://gitlab.com/webylum/puppeteer-server) service.

Please check the [documentation](docs/index.md).
