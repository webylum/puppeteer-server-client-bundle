<?php

namespace Webylum\PuppeteerServerClient\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('webylum_puppeteer_server_client');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('server')->isRequired()
                    ->children()
                        ->scalarNode('url')->isRequired()->end()
                    ->end()
                ->end()
                ->scalarNode('public_dir')->isRequired()->end()
            ->end();

        return $treeBuilder;
    }
}
