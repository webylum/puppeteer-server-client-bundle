<?php

namespace Webylum\PuppeteerServerClient\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class WebylumPuppeteerServerClientExtension extends Extension
{
    public function load(array $configs, ContainerBuilder $containerBuilder)
    {
        $loader = new XmlFileLoader(
            $containerBuilder,
            new FileLocator(__DIR__.'/../Resources/config')
        );

        $loader->load('services.xml');

        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $definition = $containerBuilder->getDefinition('webylum.puppeteer_server_client.client');
        $definition->replaceArgument('$puppeteerServerUrl', $config['server']['url']);

        $definition = $containerBuilder->getDefinition('webylum.puppeteer_server_client.twig.extension.payload');
        $definition->replaceArgument('$publicDir', $config['public_dir']);
    }
}
