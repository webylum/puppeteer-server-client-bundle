<?php

namespace Webylum\PuppeteerServerClient;

use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Webylum\PuppeteerServerClient\Exception\InvalidPayloadException;

class Payload
{
    private ClientInterface $client;
    private ?string $html = null;
    private array $pdfOptions = [];
    private array $assets = [];

    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }

    /**
     * @return FormDataPart
     *
     * Build a Symfony\Component\Mime\Part\DataPart
     * to be used by the Symfony\Component\HttpClient\HttpClient
     */
    public function toFormData(): FormDataPart
    {
        if (!$this->html) {
            throw new InvalidPayloadException($this, 'No html provided to payload.');
        }

        $fields = [
            'html' => $this->html,
        ];

        if ($this->pdfOptions !== []) {
            $fields['pdfOptions'] = json_encode($this->pdfOptions);
        }

        foreach ($this->assets as $name => $path) {
            $fields[$name] = DataPart::fromPath($path);
        }

        return new FormDataPart($fields);
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }

    /**
     * @param string $html
     * @return Payload
     */
    public function setHtml(string $html): Payload
    {
        $this->html = $html;
        return $this;
    }

    /**
     * @return array
     */
    public function getPdfOptions(): array
    {
        return $this->pdfOptions;
    }

    /**
     * @param array $pdfOptions
     * @return Payload
     */
    public function setPdfOptions(array $pdfOptions): Payload
    {
        $this->pdfOptions = $pdfOptions;
        return $this;
    }

    /**
     * @return ResponseInterface
     *
     * Send the payload
     */
    public function generate(): ResponseInterface
    {
        return $this->client->pdf($this);
    }

    /**
     * @param string $name
     * @param string $filepath
     * @return void
     *
     * Add an asset to the payload, which will be sent as a form data part.
     */
    public function addAsset(string $name, string $filepath): Payload
    {
        if (!isset($this->assets[$name])) {
            $this->assets[$name] = $filepath;
        }

        return $this;
    }
}
