<?php

namespace Webylum\PuppeteerServerClient;

use Symfony\Contracts\HttpClient\ResponseInterface;

interface ClientInterface
{
    /**
     * @return Payload
     *
     * Create a new Payload
     */
    public function createPayload() : Payload;

    /**
     * @param Payload $payload
     * @return ResponseInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     *
     * Request the Puppeteer Server to generate a pdf
     */
    public function pdf(Payload $payload) : ResponseInterface;
}
