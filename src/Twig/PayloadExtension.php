<?php

namespace Webylum\PuppeteerServerClient\Twig;

use Symfony\Component\HttpKernel\KernelInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;
use Webylum\PuppeteerServerClient\Payload;

class PayloadExtension extends AbstractExtension
{
    private string $publicDir;

    public function __construct(string $publicDir)
    {
        $this->publicDir = $publicDir;
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('wpsc_register_asset', [$this, 'registerAsset'], ['is_safe' => ['html']])
        ];
    }

    public function registerAsset(Payload $payload, string $assetUrl)
    {
        $filepath = sprintf('%s%s', $this->publicDir, $assetUrl);
        $key = sha1($assetUrl);
        $placeholder = sprintf('<##%s##>', $key);

        $payload->addAsset($key, $filepath);

        return $placeholder;
    }
}
