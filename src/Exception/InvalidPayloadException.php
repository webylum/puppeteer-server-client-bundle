<?php

namespace Webylum\PuppeteerServerClient\Exception;

use Webylum\PuppeteerServerClient\Payload;

class InvalidPayloadException extends \RuntimeException
{
    private Payload $payload;

    /**
     * @param Payload $payload
     * @param $message
     * @param $code
     * @param \Throwable|null $previous
     */
    public function __construct(Payload $payload, $message = "", $code = 0, \Throwable $previous = null)
    {
        $this->payload = $payload;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return Payload
     */
    public function getPayload(): Payload
    {
        return $this->payload;
    }
}
