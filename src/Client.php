<?php

namespace Webylum\PuppeteerServerClient;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class Client implements ClientInterface
{
    /**
     * @var string
     */
    private string $puppeteerServerUrl;

    /**
     * @var HttpClientInterface
     */
    private HttpClientInterface $client;

    /**
     * @param string $puppeteerServerUrl
     */
    public function __construct(string $puppeteerServerUrl)
    {
        $this->puppeteerServerUrl = $puppeteerServerUrl;
        $this->client = HttpClient::create([
            'base_uri' => $this->puppeteerServerUrl
        ]);
    }

    /**
     * @inheritDoc
     */
    public function createPayload(): Payload
    {
        return new Payload($this);
    }

    /**
     * @inheritDoc
     */
    public function pdf(Payload $payload): ResponseInterface
    {
        $formData = $payload->toFormData();

        return $this->client->request('POST', '/pdf', [
            'headers' => $formData->getPreparedHeaders()->toArray(),
            'body' => $formData->bodyToIterable(),
        ]);
    }

    /**
     * @param HttpClientInterface $client
     * @return void
     *
     * Update the HttpClient
     */
    public function setHttpClient(HttpClientInterface $client)
    {
        $this->client = $client;
    }
}
