<?php

namespace Webylum\PuppeteerServerClient\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Webylum\PuppeteerServerClient\Client;
use Webylum\PuppeteerServerClient\Payload;

class ClientTest extends TestCase
{
    private Client $client;

    public function setUp(): void
    {
        $this->client = new Client('http://localhost');
    }

    /**
     * @covers Client::createPayload
     */
    public function testCreatePayload()
    {
        $payload = $this->client->createPayload();

        $this->assertInstanceOf(Payload::class, $payload);
    }

    /**
     * @covers Client::pdf
     */
    public function testPdf()
    {
        $payloadStub = $this->createMock(Payload::class);
        $payloadStub->expects($this->once())->method('toFormData')->willReturn(new FormDataPart());

        $httpResponseStub = $this->createStub(ResponseInterface::class);
        $httpResponseStub->method('getHeaders')->willReturn(['content-type' => ['application/pdf']]);

        $httpClientStub = $this->createMock(HttpClientInterface::class);
        $httpClientStub
            ->expects($this->once())
            ->method('request')
            ->willReturn($httpResponseStub);

        $this->client->setHttpClient($httpClientStub);

        $response = $this->client->pdf($payloadStub);

        $this->assertInstanceOf(ResponseInterface::class, $response);
        $contentTypeHeaderValue = $response->getHeaders()['content-type'][0];
        $this->assertEquals('application/pdf', $contentTypeHeaderValue);
    }
}
