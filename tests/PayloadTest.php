<?php

namespace Webylum\PuppeteerServerClient\Tests;

use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Webylum\PuppeteerServerClient\Client;
use Webylum\PuppeteerServerClient\Exception\InvalidPayloadException;
use Webylum\PuppeteerServerClient\Payload;

class PayloadTest extends TestCase
{
    public function setUp(): void
    {
        $client = $this->createMock(Client::class);
        $this->client = $client;
    }

    /**
     * @covers Payload::generate
     */
    public function testPayloadGenerate()
    {
        $httpResponseStub = $this->createMock(ResponseInterface::class);

        $this->client->expects($this->once())
            ->method('pdf')
            ->willReturn($httpResponseStub);

        $payload = new Payload($this->client);
        $payload->generate();
    }

    /**
     * @covers Payload::toFormData
     */
    public function testToFormData()
    {
        $payload = new Payload($this->client);
        $html = bin2hex(random_bytes(150));
        $payload->setHtml($html);

        $formData = $payload->toFormData();
        $this->assertCount(1, $formData->getParts());

        $payload->setPdfOptions(['option' => 'value']);

        $formData = $payload->toFormData();
        $this->assertCount(2, $formData->getParts());

        $payload->addAsset('NAME', 'PATH');
        $formData = $payload->toFormData();
        $this->assertCount(3, $formData->getParts());

        $payload->addAsset('NAME2', 'PATH');
        $formData = $payload->toFormData();
        $this->assertCount(4, $formData->getParts());
    }

    /**
     * @covers Payload::toFormData
     */
    public function testToFormDataErrorIfNoHtml()
    {
        $payload = new Payload($this->client);
        $this->expectException(InvalidPayloadException::class);
        $payload->toFormData();
    }
}
